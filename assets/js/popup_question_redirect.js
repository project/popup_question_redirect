/**
 * @file
 * This file contains all jQuery for the forms.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.popup_question_redirect = {
    attach: function (context, settings) {

      // Wait for document to load.
      $(document).ready(function () {

        // Get the variables for the modal.
        var html = settings.hasOwnProperty('html') ? settings.html : '';
        var token = settings.hasOwnProperty('token') ? settings.token : '';

        // Get the location to redirect the visitor to.
        var location = settings.hasOwnProperty('location') ? settings.location : '';
        var location_redirect = settings.hasOwnProperty('location_redirect') ? settings.location_redirect : '';

        // Append the modal to the body.
        $('body').append(html);

        // Check the close button.
        if ($('#popup-question-redirect .close-button').length > 0) {

          // Trigger on all buttons.
          $('.button-yes, .button-hide, .button-no').on('click', function () {

            // Get the data type.
            var type = $(this).attr('data-type');

            // Update the bookmark status for the user.
            $.post(Drupal.settings.basePath + 'disable-popup', {
              'token': token,
              'type': type
            }, function () {

              // Remove the popup.
              $('#popup-question-redirect').remove();

              // Check for the yes button.
              if (type == 1) {

                // Redirect the the location.
                window.location.href = location_redirect;
              }
            });
          });
        }
      });
    }
  }
})(jQuery);
