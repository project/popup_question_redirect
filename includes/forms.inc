<?php

/**
 * @file
 * This file holds the form functions for the module.
 */

/**
 * Form constructor for the form that clears out the table.
 *
 * @see popup_question_redirect_clear_submit()
 *
 * @ingroup forms
 */
function popup_question_redirect_clear_database_form($form) {
  $form['popup_question_redirect_clear'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Clear popup question redirect entries'),
    '#description' => t('This will permanently remove the ip entries from the database.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['popup_question_redirect_clear']['clear'] = array(
    '#type'   => 'submit',
    '#value'  => t('Clear entries'),
    '#submit' => array('popup_question_redirect_clear_submit'),
  );

  return $form;
}

/**
 * Form submission handler for popup_question_redirect_clear_database_form().
 */
function popup_question_redirect_clear_submit() {
  db_delete('popup_question_redirect')->execute();
  drupal_set_message(t('Entries cleared.'));
}
