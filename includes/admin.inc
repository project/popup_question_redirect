<?php

/**
 * @file
 * This file holds the configuration form.
 */

/**
 * Form constructor for the configuration form.
 */
function popup_question_redirect_config_form() {

  // Get the configuration for the message.
  $message = variable_get('popup_question_redirect_popup_message', '');
  $message = is_array($message) && array_key_exists('value', $message) ? $message['value'] : $message;

  // Create the form array with fields.
  $form = array(
    'popup_question_redirect_popup_title'               => array(
      '#title'         => t('Popup title.'),
      '#description'   => t('The title of the popup.'),
      '#type'          => 'textfield',
      '#default_value' => variable_get('popup_question_redirect_popup_title', t('Popup title')),
    ),
    'popup_question_redirect_popup_show_logged_in'      => array(
      '#title'         => t('Show to logged in users'),
      '#type'          => 'checkbox',
      '#default_value' => variable_get('popup_question_redirect_popup_show_logged_in', 0),
      '#description'   => t('Would you like to show the popup to logged in users?'),
    ),
    'popup_question_redirect_popup_message'             => array(
      '#title'         => t('Popup message.'),
      '#description'   => t('The message of the popup. If left blank, no popup will be shown.'),
      '#type'          => 'text_format',
      '#default_value' => $message,
    ),
    'popup_question_redirect_popup_button_yes'          => array(
      '#title'         => t('Yes button text.'),
      '#description'   => t('The text on the yes button.'),
      '#type'          => 'textfield',
      '#default_value' => variable_get('popup_question_redirect_popup_button_yes', t('Yes')),
    ),
    'popup_question_redirect_popup_button_yes_location' => array(
      '#title'         => t('Yes button location.'),
      '#description'   => t('The URL to redirect a user to when pressed "Yes".'),
      '#type'          => 'textfield',
      '#default_value' => variable_get('popup_question_redirect_popup_button_yes_location', 'http://'),
    ),
    'popup_question_redirect_popup_button_hide'         => array(
      '#title'         => t('Remind me later button text.'),
      '#description'   => t('The text on the "No, remind me later" button'),
      '#type'          => 'textfield',
      '#default_value' => variable_get('popup_question_redirect_popup_button_hide', t('No, remind me later')),
    ),
    'popup_question_redirect_popup_button_no'           => array(
      '#title'         => t('No longer show button text.'),
      '#description'   => t('The text on the "No longer show button" button.'),
      '#type'          => 'textfield',
      '#default_value' => variable_get('popup_question_redirect_popup_button_no', t("No, don't show again")),
    ),
    'popup_question_redirect_ip_blacklist'              => array(
      '#title'         => t('Popup ip blacklist.'),
      '#description'   => t('The list of IP addresses to never show the popup on, one per line.'),
      '#type'          => 'textarea',
      '#default_value' => variable_get('popup_question_redirect_ip_blacklist', ''),
    ),
    'popup_question_redirect_reminder_timeout'          => array(
      '#title'         => t('Popup reminder timeout.'),
      '#description'   => t('The amount of time the popup will be hidden before it shows again.'),
      '#type'          => 'select',
      '#options'       => array(
        '00_15' => 15 . '  ' . t('minutes'),
        '00_30' => 30 . '  ' . t('minutes'),
        '01_00' => 1 . '  ' . t('hour'),
        '06_00' => 6 . ' ' . t('hour'),
        '12_00' => 12 . ' ' . t('hour'),
        '24_00' => 24 . ' ' . t('hour'),
      ),
      '#default_value' => variable_get('popup_question_redirect_reminder_timeout', '00_30'),
    ),
  );

  // Save the settings in the variable table.
  return system_settings_form($form);
}
